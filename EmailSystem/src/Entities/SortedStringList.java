
package Entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 * @author Jorge Vilaseco Jurado
 * @version 1.0
 */
public class SortedStringList{
    private Map<Integer,Mail> content = new HashMap();

    public SortedStringList() {
        
    }

    public ArrayList<Mail> getContent() {
        return toList();
    }

    public void setContent(Map<Integer, Mail> content) {
        this.content = content;
    }
    public void addMail(Mail email, int index){
        this.content.put(index, email);
    }
    /**
     * Convert a SortedArrayList to ArrayList.
     * it takes the SortedArrayList, sorted it by numbers of hits and put it into 
     * a normal ArrayList
     * @return a sorted arryList by numbers of hits
     */
    private ArrayList<Mail> toList(){
        TreeSet<Integer> keys = (TreeSet<Integer>)content.keySet();
        Iterator<Integer> myHits = keys.descendingIterator();
         
        ArrayList<Mail> myList = new ArrayList<>();
        
        myHits.forEachRemaining((value) -> {
            myList.add(content.get(value));
        });
        return myList;
    }
}
