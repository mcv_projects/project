
package Entities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 *
 * @author Jorge Vilaseco Jurado
 * @version 1.0
 * 
 */
public class Mail implements Serializable{
    private static int mailIdBase = 1;
    private final int mailId;
    private String senderEmail;
    private String receiverEmail;
    private String title;
    private String content;
    private boolean read = false;
    private boolean sent = false;
    private int count;
    
    public Mail(String newSenderEmail,String newReceiverEmail, String newTitle, String newContent){
        this.mailId = mailIdBase;
        this.senderEmail = newSenderEmail;
        this.receiverEmail = newReceiverEmail;
        this.content = newContent;
        this.title = newTitle;
        mailIdBase++;
    }

    public String getSender() {
        return senderEmail;
    }

    public void setSender(String sender) {
        this.senderEmail = sender;
    }

    public String getReceiver() {
        return receiverEmail;
    }

    public void setReceiver(String receiver) {
        this.receiverEmail = receiver;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static int getMailIdBase() {
        return mailIdBase;
    }
    public static void setMailIdBase(int mailIdBase) {
        Mail.mailIdBase = mailIdBase;
    }
    public int getMailId() {
        return mailId;
    }
    
    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }
    /**
     * Search word contained in the email.
     * Search sender and receiver email addresses , title and content of the email
     * 
     * @param toMacth word or sentence to search within the email
     * @return return the number of hits in the search
     */
    public int matchSearch(String toMacth){
        Set<String> allContent = new HashSet<>();
        
        String[] titleWords = this.title.split(" ");
        String[] contentWords = this.content.split(" ");
        List<String> macthWords = Arrays.asList(toMacth.split(" "));
        //  Add all Strigns without duplicates to the Set 
        allContent.addAll(Arrays.asList(titleWords));
        allContent.addAll(Arrays.asList(contentWords));

        macthWords.forEach(new Consumer<String>() {
            int counter = 0;
            @Override
            public void accept(String macthWord) {
                if(allContent.contains(macthWord)){
                    counter++;
                    sendCounterBack(counter);
                }
            }
        });
        return count;
    }
    /**
     * Sets count of hits in the search
     * 
     * @param newCount Counter hits from the search 
     */
    public void sendCounterBack(int newCount){
        this.count = newCount;
    }
    
    public String displayFullMail(){
        return "Sender Email: " + this.senderEmail +'\n'+"Title: " + this.title +'\n'+"Content: " + this.content+'\n';
    }
    
    @Override
    public String toString(){
        return "Email ID: "+this.mailId+'\n'+"Sender: "+this.senderEmail+'\n'+"Title: "+this.title+'\n';
    }
}
