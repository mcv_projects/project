
package Entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Jorge Vilaseco Jurado
 * @version 1.0
 */



public class Inbox implements Serializable{
    ArrayList<Mail> mailList;
    boolean found = false;
    
    public Inbox(){
        mailList = new ArrayList<>();
    }

    public Inbox(ArrayList<Mail> mailList) {
        this.mailList = mailList;
    }

    public ArrayList<Mail> getMailList() {
        return mailList;
    }

    public void setMailList(ArrayList<Mail> mailList) {
        this.mailList = mailList;
    }
    /**
     * 
     * @param match Word or sentence to match
     * @return return ArrayList containing the mails that match, an empty ArrayList otherwise
     */
    public ArrayList<Mail> searchMails(String match){
        SortedStringList mails = new SortedStringList();
        
        mailList.forEach((mail) -> {
            int hits = mail.matchSearch(match);
            if (hits != 0){
                mails.addMail(mail,hits);
            }
        });
        return mails.getContent();
    }
    /**
     * 
     * @return ArrayList with unread mails, empty ArrayList if all read
     */
    public ArrayList<Mail> getNewMails(){
        ArrayList<Mail> mails = new ArrayList<>();
        
        mailList.forEach((mail) -> {
            if (!mail.isSent()){
                mails.add(mail);
                mail.setSent(true);
            }
        });
        return mails;
    }
    /**
     * Add the new mails to the received 
     * 
     * @param newMails ArrayList with the new mails
     */
    public void addNewMail(ArrayList<Mail> newMails){
        
        newMails.forEach((mail) -> {
            if (!mail.isSent()){
                mailList.add(mail);
            }
        });
    }
    public void addNewMail(Mail newMail){
        mailList.add(newMail);
    }
    /**
     * 
     * @param mailId int to search for the email
     * @return return true if the mail was deleted, false otherwise
     */
    public boolean deleteMail(int mailId){
        
        mailList.forEach(mail->{
            if(mail.getMailId() == mailId){
                mailList.remove(mail);
                setFound(true);
            }
        });
        
        return found;
    }
    private void setFound(boolean newState){
        this.found = true;
    }
    
    public Mail openMail(int mailId){

        Mail[] myMail = {null};
        
        mailList.forEach(mail->{
            if(mail.getMailId() == mailId){
                myMail[0] = mail;
            }
        });
        return myMail[0];
    }
    
    @Override
    public String toString(){
        return mailList.toString();
    }
}
