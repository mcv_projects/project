
package Entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author edricajasmine
 */ 
public class UserList implements Serializable{
    
    ArrayList<User> listOfUsers = new ArrayList();
        
    public UserList(){

    }
    
    public UserList(ArrayList<User> listOfUsers){
        this.listOfUsers = listOfUsers;
    }
    
    public boolean addUser(User aUser){
        listOfUsers.add(aUser);
        return true;
    }
    
    public boolean deleteUser(User aUser){
        
        listOfUsers.remove(aUser);
        
        return true;
    }
    
    public ArrayList<User> getUserList(){
        return listOfUsers;
    }
    /**
     * Search for an user.
     * the method look for a match by emailAddress and password
     * 
     * @param emailAddress is the user's email address 
     * @param password is the user's password 
     * @return an User object, otherwise null
     */
    public User searchUser(String emailAddress, String password){
        User aUser = null;
        int index = 0;
        boolean found = false;
        
        while(index < listOfUsers.size() && found == false){
            if(emailAddress.equals(listOfUsers.get(index).getEmailAddress())){
                if(password.equals(listOfUsers.get(index).getPassword())){
                    aUser = listOfUsers.get(index);
                    found = true;
                }
            }
            index++;
        }
        return aUser;
    }
    
    public User searchUser(String emailAddress){
        User aUser = null;
        int index = 0;
        boolean found = false;
        
        while(index < listOfUsers.size() && found == false){
            if(emailAddress.equals(listOfUsers.get(index).getEmailAddress())){
                aUser = listOfUsers.get(index);
                found = true;
            }
            index++;
        }
        return aUser;
    }
}
