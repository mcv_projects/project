
package Entities;

/**
 *
 * @author edricajasmine
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class User implements Serializable{
    
    private int user_id;
    static int userIdCount = 17000;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private Inbox userReceivedMails;
    private Inbox userSentMails;
    private int userType = 0;
   
    public User(String newFirstName, String newLastName, String newEmailAddress, String newPassword){
        setUser_id();
        this.firstName = newFirstName;
        this.lastName = newLastName;
        this.emailAddress = newEmailAddress;
        this.password = newPassword;
        this.userReceivedMails = new Inbox();
        this.userSentMails = new Inbox();
        
    }

    public int getUser_id() {
        return this.user_id;
    }

    //"userIdCount" increments everytime current "userIdCount" is assigne"d to "user_id".
    public final void setUser_id() {
        this.user_id = userIdCount;
        userIdCount++;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String newFirst_name) {
        this.firstName = newFirst_name;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLast_name(String newLast_name) {
        this.lastName = newLast_name;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public void setEmailAddress(String newEmail_address) {
        this.emailAddress = newEmail_address;
    }
    
    public Inbox getUserReceivedMails() {
        return userReceivedMails;
    }

    public void setUserReceivedMails(Inbox userReceivedMails) {
        this.userReceivedMails = userReceivedMails;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
    
    /**
     * 
     * @param newMail is an Mail Object the user received, that needs to be added to the array list of receivedMails of the user.
     * @return a boolean to certify the mail was added
     */
    public boolean addUserReceivedMail(Mail newMail){
        
        this.userReceivedMails.addNewMail(newMail);
        
        return true;
    }
    /**
     * 
     * @param newMail is an Mail Object the user sent, that needs to be added to the array list of sentMails of the user.
     * @return a boolean to certify the mail was added
     */
    public boolean addUserSentMail(Mail newMail){
        
        this.userSentMails.addNewMail(newMail);
        
        return true;
    }
    
    public Inbox getUserSentMails() {
        return userSentMails;
    }

    public void setUserSentMails(Inbox userSentMails) {
        this.userSentMails = userSentMails;
    }
    
    /**
     * 
     * @param newEmailAddress is the user's email address
     * @return a validation message that contains "Valid", or specific error message regarding the email address format.
     */
    public static String checkEmailValidation(String newEmailAddress) throws ArrayIndexOutOfBoundsException {
        String validation = "Please enter a valid email address.";
        String[] userName = newEmailAddress.split("@");
        
        if(newEmailAddress.isEmpty()){
            validation = "Please enter an email address.";
        }
        else if(!userName[0].isEmpty() && !userName[1].isEmpty()) {
            
                String[] emailDomain = userName[1].split("\\.");
            
                if(!emailDomain[0].isEmpty()){
                    
                    if(emailDomain[1].length() >=2 && emailDomain[1].length() <= 4)
                        validation = "Valid";
                    else
                        validation = emailDomain[1] + " is invalid";
                }
            
        }
        return validation;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }
    /**
     * 
     * @param newFirstName is the user's first name.
     * @param newLastName is the user's last name.
     * @param newPassword is the user's password.
     * @param newEmailAddress is user's email address.
     * @return a validation message that contains either a "Valid", or specific error message regarding the password security.
     */
    public static String checkPasswordValidation(String newFirstName, String newLastName, String newEmailAddress, String newPassword){
        String validation = "Valid";
        int minimum_length = 6;
        String[] newUserName = newEmailAddress.split("@");
        
        if(newPassword.length() < minimum_length){
            validation = "Password must contain atleast "+minimum_length+" characters.";
        }
        else if(newPassword.equalsIgnoreCase(newFirstName + newLastName)){
            validation = "Password must not be the same as user's first and last name.";
        }
        else if(newPassword.equalsIgnoreCase(newUserName[0])){
            validation = "Password must not match username's email address.";
        }
        
        return validation;
    }
    
    /**
     * 
     * @param mailId int to search for the email
     * @return return true if the mail was deleted, false otherwise
     */
    public boolean deleteMail(int mailId){
        
        boolean found = userReceivedMails.deleteMail(mailId);
        if(!found){
            found = userSentMails.deleteMail(mailId);
        }
        return found;
    }
    /**
     * 
     * @param mailId int for the mail id
     * @return A received Mail
     */
    public Mail openReceivedMail(int mailId){
        return userReceivedMails.openMail(mailId);
    }
    
    /**
     * Search for Mail.
     * Search for an mail in the sent Inbox and return it if found
     * 
     * @param mailId int for the mail id
     * @return A sent Mail, null otherwise
     */
    public Mail openSentMail(int mailId){
        return userSentMails.openMail(mailId);
    }
    
    public ArrayList<Mail> searchMails(String keywords){
        ArrayList<Mail> result;
        
        result = userReceivedMails.searchMails(keywords);
        result.addAll(userSentMails.searchMails(keywords));
        
        return result;
    }   
    
    public ArrayList<Mail> getNewMails(){
        ArrayList<Mail> newMails;
        
        newMails = userReceivedMails.getNewMails();
        
        return newMails;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.emailAddress);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.emailAddress, other.emailAddress)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User Account: \n" + "First Name:" + this.firstName + "\nLast Name:" + this.lastName + "\nEmail Address:" + this.emailAddress;
    }
    
    
    
}
