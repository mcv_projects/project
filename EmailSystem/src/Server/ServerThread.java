
package Server;

import Entities.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author Jorge Vilaseco Jurado
 */
public class ServerThread implements Runnable{
    
    private ObjectOutputStream toClient;
    private ObjectInputStream fromClient ;
    Socket myClientSocket;
    User currentUser = null;
    boolean keepConn = true;
    
    public ServerThread(Socket clientSocket){
        try {
            myClientSocket = clientSocket;
            /**
             * Stage 4.
             * Create the reader and the writer objects to receive and send
             * data through the stream create for one specific client
             *
             * - ObjectInputStream and ObjectOutputStream for handling Java Objects
             */
            
            toClient = new ObjectOutputStream(myClientSocket.getOutputStream());
            fromClient = new ObjectInputStream(myClientSocket.getInputStream());
            /**
             * Loop for the connection with the specific client
             * in which the logic of the server is implemented
             */
        } catch (IOException ex) {
            System.out.println("Problem to create the socket "+ ex.getMessage());
        }

    }
    @Override
    public void run() {
        
        while(keepConn){

            String stringResponse = "";
            try {
                //  Read from the Stream

                Object input = fromClient.readObject();

                //  Server Logic
                switch(input.getClass().getName()){
                    case "java.lang.String":
                        //  Cast input to String
                        String requestString = (String)input;
                        //  Split the request to analise it
                        String[] request = requestString.split("%%");
                        System.out.println(Arrays.toString(request));
                        switch(request[0]){
                            case "LOGIN":
                                System.out.println("step 1");
                                if(request.length == 3){
                                    System.out.println("step 2");
                                    User newUser = Server.getUsers().searchUser(request[1],request[2]);
                                    System.out.println("step 3");
                                    if(newUser != null){
                                        System.out.println("step 4");
                                        currentUser = newUser;
                                        sendResponse(currentUser);
                                        System.out.println("user logged in"+currentUser);
                                        
                                    }else{
                                        stringResponse = "FAILED";
                                        //  Send String response
                                        sendResponse(stringResponse);
                                    }
                                }else{
                                    stringResponse = "INVALID_REQUEST";
                                    //  Send String response
                                    sendResponse(stringResponse);
                                }
                                System.out.println(stringResponse);
                                break;
                            case "DELETE":
                                if(currentUser != null){
                                    //  Delete Mail
                                    if(request.length == 2){
                                        try{
                                            int id = Integer.parseInt(request[1]);

                                            if(currentUser.deleteMail(id)){
                                                stringResponse = "DELETED";
                                                //  Send String response
                                                sendResponse(stringResponse);
                                            }else{
                                                stringResponse = "NOT_DELETED";
                                                //  Send String response
                                                sendResponse(stringResponse);
                                            }
                                        }catch (NumberFormatException e){
                                            stringResponse = "INVALID_ID";
                                            //  Send String response
                                            sendResponse(stringResponse);
                                        }

                                    }
                                }else{
                                    stringResponse = "ACCESS_DENIED";
                                    //  Send String response
                                    sendResponse(stringResponse);
                                }
                                break;
                            case "LOG_OUT":
                                sendResponse(logOut());
                                break;
                            case "EXIT":
                                exitServer();
                                break;
                            case "SHUT_DOWN":
                                if(currentUser.getUserType() == 0){
                                    sendResponse(shutDownServer());
                                }else{
                                    sendResponse("ACCESS_DENIED");
                                }
                                break;
                            case "GET_NEW_MAILS":
                                if(currentUser != null){
                                    ArrayList<Mail> newMails = currentUser.getUserReceivedMails().getNewMails();
                                    //  Send ArrayList respone
                                    sendResponse(newMails);
                                }else{
                                    stringResponse = "ACCESS_DENIED";
                                    //  Send String response
                                    sendResponse(stringResponse);
                                }
                                break;
                            default:
                                stringResponse = "INVALID_REQUEST";
                                sendResponse(stringResponse);
                        }
                        break;

                    case "Entities.User":
                        //Resgister a new User
                        User newUser = (User)input;
                        System.out.println("Request from Client: " +newUser);
                        boolean registerResponse = registerNewUser(newUser);

                        if (registerResponse) {
                            stringResponse = "SUCCESSFUL";
                        }else{
                            stringResponse = "FAILED";
                        }

                        sendResponse(stringResponse);

                        break;
                    case "Entities.Mail":
                        //  Send a email
                        if(currentUser != null){
                            Mail myMail = (Mail)input;
                            if(myMail.getSender().equals(currentUser.getEmailAddress())){
                                //  Add received email to the receiver user
                                Server.myUsers.searchUser(myMail.getSender()).addUserReceivedMail(myMail);
                                // Add mail sent to the sender user
                                currentUser.addUserSentMail(myMail);
                                
                                stringResponse = "SENT";
                                //  Send String response
                                sendResponse(stringResponse);
                            }else{
                                stringResponse = "NOT_SENT";
                                //  Send String response
                                sendResponse(stringResponse);
                            }
                        }else{
                            stringResponse = "ACCESS_DENIED";
                            //  Send String response
                            sendResponse(stringResponse);
                        }
                        break;
                    default:
                        stringResponse = "INVALID_REQUEST";
                        sendResponse(stringResponse);
                }

                //  Response to the client if required
            } catch (IOException ex) {
                System.out.println("Problem to send response to client "+ ex.getMessage());
            } catch (ClassNotFoundException ex) {
                System.out.println("Resquest got corrupted "+ ex.getMessage()+ex.getLocalizedMessage());
            }
            
        }  
    }
    
    private void sendResponse(Object response) throws IOException {
        toClient.writeObject(response);
    }

    /**
     * Register a new User.
     * The method checks if the user was already created and 
     * add it if it isn't created already 
     * 
     * @param newUser User object to add to the list
     * @return return true if it was added, false otherwise
     */
    private boolean registerNewUser(User newUser){
        Set<User> myUsersSet = new HashSet(Server.getUsers().getUserList());
        if(myUsersSet.contains(newUser)){
            return false;
        }else{
            if(Server.getUsers().addUser(newUser)){
                try {
                    Server.writeCustBinFile();
                    return true;
                } catch (IOException ex) {
                    
                }
            }
            return false;
        }
    }

    private String exitServer() {
        try {
            Server.writeCustBinFile();
            this.keepConn = false;
            
            return "COMFIRMED";
        } catch (IOException ex) {
            return "ERROR";
        }
    }
    private String logOut() {
        try {
            //  Save the data into the files
            Server.writeCustBinFile();
            currentUser = null;
            return "BYE";
        } catch (IOException ex) {
            return "ERROR";
        }
    }
    
    private String shutDownServer() {
        while(Thread.activeCount() > 2){
        }
        if(exitServer().equals("ERROR")){
            
            Server.keepRunning = false;
            return "Done";
        }
        return "ERROR";
        
    }
}
