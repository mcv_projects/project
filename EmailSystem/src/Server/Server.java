
package Server;

import Entities.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Jorge Vilaseco Jurado
 * @version 1.0
 * 
 */
public class Server {
    //  Static declaration of global variables to be use by the server
    static final int MYPORT = 31106;
    static UserList myUsers;
    static  boolean keepRunning = true;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            /**
             * Step 1.
             * Create a socket to listen connection requests
             * It has to be a server socket, it manages the handshake
             * and stablish the connection between server and client
             * data sockets, IPs, Ports, etc.
             */
            ServerSocket myServerSocket = new ServerSocket(MYPORT);
            System.out.println("Waiting for first connection request...");
            
            //read the user file where user are stored
            myUsers =  readCustFile();
            //  Loop to keep running the server
            while(keepRunning){
                /**
                 * Stage 2.
                 * Create the data socket after the handshake done by the method 
                 * accept() of ServerSocket.
                 */
                Socket myClientSocket = myServerSocket.accept();
                System.out.println("Connection accepted!");
                
                /**
                 * Stage 3.
                 * Create a thread sending the information of the client to it
                 */
                ServerThread runnableThread = new ServerThread(myClientSocket);
                Thread newThread = new Thread(runnableThread);
                //  Start the thread 
                newThread.start();
            }
            myServerSocket.close();
            
        } catch (FileNotFoundException | ClassNotFoundException e) {
            System.out.println("Problems to load the users");
            System.out.println(e.getMessage());
        } catch (IOException  e) {
            System.out.println("Problems to create the server socket");
            System.out.println(e.getMessage());
        }
    }
    
    static UserList readCustFile() throws FileNotFoundException, IOException, ClassNotFoundException {
       ObjectInputStream readCust = new ObjectInputStream(new FileInputStream("userList.bin"));
        UserList result = (UserList)readCust.readObject();
        if(result != null){
            return result;
        }
       return new UserList();
        
    }
    static void writeCustBinFile() throws FileNotFoundException, IOException {
        //  Loads the file to be able to write on it the customers as Objects
        ObjectOutputStream toCustomerFile = new ObjectOutputStream(new FileOutputStream("userList.bin"));
        //  Goes through all customer putting then into the file
        toCustomerFile.writeObject(myUsers);
        //  Close the connection to the file to free it
        toCustomerFile.close();
    }
    static UserList getUsers(){
        return myUsers;
    }
}
