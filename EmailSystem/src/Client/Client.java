

package Client;

/**
 *
 * @author edricajasmine
 */

import Entities.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    
    static ObjectInputStream fromServer;
    static ObjectOutputStream toServer;
    static Scanner input = new Scanner(System.in);
    static User currentUser;
    static Mail currentMail;
    static Object request;
    static Object response;
    
    public static void main(String[] args){
        final int serverPort = 31106;
        
        try {
            
            //Create a socket
            Socket serverLink = new Socket("localhost",serverPort);
            
            // Build the objects used to interact with the server through their input and output streams            
            toServer = new ObjectOutputStream(serverLink.getOutputStream());
            fromServer = new ObjectInputStream(serverLink.getInputStream());
        
            
            int choice = -1;
            
            while(choice != 3 && response != "BYE"){
                
                choice = mainMenu();
                
                switch(choice){
                    case 1: 
                        registerUser();
                        break;
                    case 2: 
                        login();
                        break;
                    case 3:
                        exit();
                        break;
                    default:
                        System.out.println("Invalid input!");
                }
            }
            fromServer.close();
            toServer.close();
            serverLink.close();
        } catch (IOException ex) {
            System.out.println("Problem to connect to the server " + ex.getMessage());
        } catch (ClassNotFoundException | IllegalArgumentException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
   
    /**
     * This method sends the User's request to the server
     * 
     * @param request contains the request of the current command of the user
     * @throws IOException 
     */
    public static void sendRequest(Object request) throws IOException{
        System.out.println("Request to Server: " + request);
        toServer.writeObject(request);
        toServer.flush();
    }
    
    /**
     * This method receives the response from the server to the User's request
     * 
     * @return the Object of response from the User's request
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static Object receiveRequest() throws IOException, ClassNotFoundException{
        response = fromServer.readObject();
        System.out.println("Response from Server: " + response);
        return response;
    }
    
    /**
     * The method that displays the main menu
     * 
     * @return an integer of selected choice of action
     */
    public static int mainMenu(){
        int choice = 0;
        
        System.out.println("\nMenu: ");
        System.out.println("\n1] Registration");
        System.out.println("\n2] Login");
        System.out.println("\n3] Exit");
        System.out.print("\nInput choice: ");
        
        if(input.hasNextInt()) 
            choice = input.nextInt();
        
        input.nextLine();
        
        return choice;
    }
    
    /**
     * The method for the application to display for User to log in
     * 
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public static void login() throws IOException, ClassNotFoundException{
        request = "LOGIN";
        String emailAddress;
        String password;
        
        System.out.println("Login Account");
        System.out.print("Email address: ");
        emailAddress = input.nextLine().trim();
        System.out.print("Password: ");
        password = input.nextLine().trim();
                
        sendRequest((String)request +"%%" + emailAddress + "%%" + password);
        
        response = receiveRequest();
        getServerResponseAndAction(response, 0);
    }
    
    /**
     * The method for the application to display registration fields with validations on the User's input
     * 
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public static void registerUser() throws IOException, ClassNotFoundException{
        
        System.out.println("Register");
        
        String newFirstName = "";
        String newLastName = "";
        String newEmailAddress;
        String newPassword;
        String emailValidation;
        String passwordValidation;
        
        System.out.print("\nFirst name: ");
        //validates the first name of the user
        if(!input.hasNextInt()) 
             newFirstName = input.nextLine();
        else {
            System.out.println("First name can't have numbers.");
            input.nextLine();
            registerUser();
        }
        
        System.out.print("\nLast name: "); 
        //validates the last name of the user
        if(!input.hasNextInt()) 
             newLastName = input.nextLine();
        else {
            System.out.println("Last name can't have numbers.");
            registerUser();
        }
        
        System.out.print("\nEmail Address: ");
        newEmailAddress = input.nextLine();
        //validates the email address of the user
        emailValidation = User.checkEmailValidation(newEmailAddress);
        if(!emailValidation.equalsIgnoreCase("valid")){
            System.out.println(emailValidation);
            registerUser();
        }
        
        System.out.print("\nPassword: ");
        newPassword = input.nextLine();
        //validates the password of the user
        passwordValidation = User.checkPasswordValidation(newFirstName, newLastName, newEmailAddress, newPassword);
        
        if(!passwordValidation.equalsIgnoreCase("valid")){
            System.out.println(passwordValidation);
            registerUser();
        }
        
        User aUser = new User(newFirstName, newLastName, newEmailAddress, newPassword);
        
        request = aUser;
        sendRequest(request);
        response = fromServer.readObject();
        
        getServerResponseAndAction(response, 1);
    }
    
    /**
     * The method that exits the application from the main menu
     * 
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void exit() throws IOException, ClassNotFoundException{
        request = "EXIT";
        sendRequest(request);
        
        response = receiveRequest();
        getServerResponseAndAction(response, 0);
        response = fromServer.readObject();
        
        getServerResponseAndAction(response,1);
    }
    
    /**
     * The method that displays the logged in menu once a user is logged in successfully
     * 
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public static void loggedInMenu() throws IOException, ClassNotFoundException{
        int menu = 0;
        
        System.out.println("\n1] Unread mails");
        System.out.println("\n2] View mails");
        System.out.println("\n3] Search mail/s");
        System.out.println("\n4] Send mail");
        System.out.println("\n5] Delete mail");
        System.out.println("\n6] Logout");
        System.out.println("\n7] Shut Down");
        System.out.print("\nInput choice: ");
        
        if(input.hasNextInt()){
            menu = input.nextInt();
            caseLoggedInMenu(menu);
        }
        else {
            System.out.println("Invalid input!");
            loggedInMenu();
        }
        input.nextLine();
    }
    
    /**
     * The method that receives the User's action from the choices of the loggedInMenu() 
     * 
     * @param menu is the variable that contains the User's choice of input.
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void caseLoggedInMenu(int menu) throws IOException, ClassNotFoundException{
        
        while( menu != 7) {
            switch(menu){
                case 1: 
                    request = "GET_NEW_MAILS";
                    retrieveNewMails();

                    break;
                case 2: 
                    viewMails();

                    break;
                case 3:
                    System.out.println("Search keyword or phrase: ");
                    String keywords = input.nextLine();
                    searchMails(keywords);

                    break;
                case 4:
                    request = "SEND";
                    sendMail();

                    break;
                case 5: 
                    request = "DELETE";
                    deleteMail();

                    break;
                case 6:
                    request = "LOG_OUT";
                    logout();

                    break;
                case 7:
                    request = "SHUT_DOWN";
                    shutDown();
                    
                    break;
                default:
                    System.out.println("Invalid input!");
            }
            loggedInMenu();
        }
    }
    
    /**
     * The method that retrieves new mails of the current User of the application, along with the sending request and receiving response
     * 
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void retrieveNewMails() throws IOException, ClassNotFoundException{
        
        sendRequest((String) request);
        
        response = receiveRequest();
        getServerResponseAndAction(response,0);
    }
    
    public static void viewMails() throws IOException, ClassNotFoundException{
        
        System.out.println("View Mails");
        System.out.println("1]Received Mails");
        System.out.println("2]Sent Mails");
        
        int view = input.nextInt();
        
        switch(view){
            case 1: 
                System.out.println(currentUser.getUserReceivedMails());
                System.out.println("1]Open mail");
                System.out.println("2]Back to menu");
                System.out.print("Input choice: ");
                
                if(input.hasNextInt()){
                    int choice = input.nextInt();
                    
                    switch(choice){
                        case 1: 
                            System.out.println("Input email id to open: ");
                            
                            if(input.hasNextInt()){
                                int emailId = input.nextInt();
                                Mail myMail = currentUser.openReceivedMail(emailId);
                                
                                if(myMail == null){
                                    System.out.println("That email doesn't exist");
                                }
                                else{
                                    currentMail = myMail;
                                    System.out.println(currentMail.displayFullMail());
                                    //  Delete mail here
                                }
                            }
                            else {
                                System.out.println("Invalid input");
                                viewMails();
                            }
                            break;
                    }
                }
                break;
            case 2:
                System.out.println(currentUser.getUserSentMails());
                System.out.println("1]Open mail");
                System.out.println("2]Back to menu");
                System.out.print("Input choice: ");
                
                if(input.hasNextInt()){
                    int choice = input.nextInt();
                    
                    switch(choice){
                        case 1: 
                            System.out.println("Input email id to open: ");
                            
                            if(input.hasNextInt()){
                                int emailId = input.nextInt();
                                Mail myMail = currentUser.openSentMail(emailId);
                                
                                if(myMail == null){
                                    System.out.println("That email doesn't exist");
                                }
                                else{
                                    currentMail = myMail;
                                    System.out.println(currentMail.displayFullMail());
                                    //  Delete mail here
                                }
                            }
                            else {
                                System.out.println("Invalid input");
                                viewMails();
                            }
                            break;
                    }
                }
                break;
            default:
                System.out.println("Invalid input!");
                viewMails();
        }
    }
    
    public static void searchMails(String keywords) throws IOException, ClassNotFoundException{
        System.out.println("Search Mails by " + keywords);
        
        ArrayList<Mail> matchedMails = currentUser.searchMails(keywords);
        System.out.println(matchedMails);
    }
    
    public static void sendMail() throws IOException, ClassNotFoundException{
        
        String senderEmail = currentUser.getEmailAddress();
        String receiverEmail;
        String newTitle;
        String newContent;
        
        System.out.println("Send a mail: ");
        System.out.print("To: ");
        receiverEmail = input.nextLine();
        input.nextLine();
        System.out.print("Tite: ");
        newTitle = input.nextLine();
        input.nextLine();
        System.out.print("Message: ");
        newContent = input.nextLine();
        
        Mail sendMail = new Mail(senderEmail,receiverEmail,newTitle,newContent);
        
        request = sendMail;
        
        sendRequest(request);
        currentMail = sendMail;
        response = receiveRequest();
        getServerResponseAndAction(response, 0);
    }
    
    public static void deleteMail() throws IOException, ClassNotFoundException{
        
        if(currentMail != null){
            request = (String)request + "%%" + Integer.toString(currentMail.getMailId());
            
            sendRequest((String)request);
            response = receiveRequest();
            getServerResponseAndAction(response, 0);
        }
        else 
            System.out.println("Please open the email before deleting it.");
    }
    
    public static void logout() throws IOException, ClassNotFoundException{
        
        sendRequest((String)request);
        response = receiveRequest();
        getServerResponseAndAction(response, 0);
    }
    
    public static void shutDown() throws IOException, ClassNotFoundException{
        
        sendRequest(request);
        
        response = receiveRequest();
        getServerResponseAndAction(response,0);
    }
    
    public static void getServerResponseAndAction(Object response, int flag) throws IOException, ClassNotFoundException{
        String serverResponse = response.getClass().getName();
        
        switch(serverResponse){
            case "Entities.User":
                currentUser = (User)response;
                System.out.println(currentUser);
                loggedInMenu();
                break;
            case "java.util.ArrayList":
                
                System.out.println("New Mails");
                ArrayList<Mail> myNewMails = (ArrayList<Mail>) response;
                System.out.println(myNewMails);
                loggedInMenu();
                break;
            case "java.lang.String":
                String responseMsg = (String)response;
                
                switch(responseMsg){
                    case "SUCCESSFUL":
                        System.out.println("Resgistration successful!");
                        System.out.println("Please Login.");
                        mainMenu();
                        break;
                    case "DONE":
                    case "CONFIRMED":
                        System.exit(0);
                        
                        break;
                    case "SENT":
                        System.out.println("Email sent!");
                        //  Adding the sent mail to the sent mail list of the current user
                        currentUser.addUserSentMail(currentMail);
                        //  Setting the current mail to null because it is not needed anymore
                        currentMail = null;
                        loggedInMenu();
                        break;
                    case "DELETED":
                        System.out.println("Email deleted!");
                        loggedInMenu();
                        break;
                    case "BYE":
                        currentUser = null;
                        currentMail = null;
                        mainMenu();
                        
                        break;
                    case "FAILED":
                        if(flag == 1){
                            System.out.println("User already exist.");
                            mainMenu();
                        }
                        else {
                            System.out.println("Email address or password is incorrect.");
                            mainMenu();
                        }
                        
                        break;
                    case "NOT_SENT":
                        System.out.println("Email address doesn't exist.");
                        //  Setting the current mail to null because it is not needed anymore
                        currentMail = null;
                        loggedInMenu();
                        break;
                    case "NOT_DELETED":
                        System.out.println("Email cannot be deleted.");
                         loggedInMenu();   
                        break;
                    case "ERROR":
                        System.out.println("The program can't save the information.");
                        loggedInMenu();
                        break;
                    case "INVALID_REQUEST":
                        System.out.println("Protocol was not followed.");
                        if(currentUser != null){
                            loggedInMenu();
                        }
                        else{
                            mainMenu();
                        }
                        break;
                    case "ACCESS_DENIED":
                        System.out.println("User have no privileges or User has to logged in.");
                        if(currentUser != null){
                            loggedInMenu();
                        }
                        else{
                            mainMenu();
                        }
                        break;
                    case "INVALID_ID":
                        System.out.println("The email id has to be a number.");
                        loggedInMenu();
                        break;
                    default:
                        mainMenu();
                }
                break;
            default:
                System.out.println("The server failed to respond.");
                currentUser = null;
                currentMail = null;
                mainMenu();
        }
        
    }
}   
